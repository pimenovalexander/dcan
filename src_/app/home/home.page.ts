import { Component } from '@angular/core';
import { CardService } from './shared/card.service';

import { CardDeck } from './shared/card.image.model';

import { HTTP } from '@ionic-native/http/ngx';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

//убрать нахер потом хттп
  constructor(private http: HTTP) {

  }

  public cardDecks: CardDeck[] = [];

  private readonly baseUrl = '​http://54.76.15.222:8090/api/';
  private readonly appId = '036597aa-2c95-4555-8ba8-4b274fbdc62d';
  private readonly imagesUrl = `${this.baseUrl}getImages?appId=${this.appId}`;

  requestObject1: any = null;

    getRequest() {
      this.http.get(this.imagesUrl, {}, {})
          .then(res => this.requestObject1 = res.data)
          .catch(err => this.requestObject1 = err);

    }

/*
  private getCardImages() {
    this.cardService.getAllCardImages().then(
      (cardDecks: CardDeck[]) => this.cardDecks = cardDecks)
  }
*/
}
