import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { HTTP } from '@ionic-native/http/ngx';

import { HttpClientModule } from '@angular/common/http';

import { HomePage } from './home.page';

import { CardService } from './shared/card.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HttpClientModule,
    HTTP,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage
      }
    ])
  ],
  providers: [
    CardService, HTTP
  ],
  declarations: [HomePage]
})
export class HomePageModule {}
