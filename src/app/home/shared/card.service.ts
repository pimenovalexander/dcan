import { Injectable } from '@angular/core';
import { of as ObservableOf, from, Observable} from 'rxjs';
//import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HTTP } from '@ionic-native/http/ngx';

import { CardDeck } from './card.image.model';

@Injectable()
export class CardService {

private readonly appId = '036597aa-2c95-4555-8ba8-4b274fbdc62d';

private readonly baseUrl = 'http://54.76.15.222:8090/api/';

private readonly imagesUrl = `${this.baseUrl}getImages?appId=${this.appId}`;

// this is URL for backandless
// tslint:disable-next-line:max-line-length

    //private readonly dummyURL = 'https://api.backendless.com/F4612721-B0B0-407B-FF3D-69C4BB8EC000/94EA88C4-04CB-E558-FF66-05FEC6FF0200/data/scanbot';

    //private readonly dummyURL = 'https://jsonplaceholder.typicode.com/todos/1';

  private readonly cardDecks: string[] = ['Card 1', 'Card 2', 'Card 3'];

    constructor(private http: HTTP) {}

// this is HttpClient - we need to convert this into native, is used in hame.page.ts
//  public getAllCardImages(): Observable<CardDeck[]> {
 public getAllCardImages() {
  const imagePromise = new Promise((resolve, reject) => {
    this.http.get(this.imagesUrl, {}, {})
//      this.http.get(this.dummyURL, {}, {})
        .then(res => {resolve(res),
        console.log(res)
        }).
        catch(err => {
              reject(err);
        });
  });

      // @ts-ignore
      return imagePromise;

    //return this.http.get<CardDeck[]>(this.imagesUrl);

  }



  public getDummyImages(): Observable<CardDeck[]> {
        return
  }

  // getRequest() {
  //     this.http.get(this.dummyURL, {}, {})
  //       .then( res => this.requestObject = res.data).
  //       catch(err => this.requestObject = err);
  // }

}
